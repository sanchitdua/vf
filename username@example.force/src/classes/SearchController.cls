public with sharing class SearchController {

    public List<COntact> cList{get;set;}
    public List<Account> aList{get;set;}

    public String aSoql{get;set;}
    public String cSoql{get;set;}
    
    public String accountRadioValue {get;set;}
    public String contactRadioValue {get;set;}
    
    public String accountInputValue {get;set;}
    public String contactInputValue {get;set;}

    public String accountSortDirection {get{if(accountSortDirection == null) accountSortDirection = 'asc'; return accountSortDirection ;}
        set;
    }

    public String contactSortDirection {get{if(contactSortDirection == null) contactSortDirection = 'asc'; return contactSortDirection ;}
        set;
    }


    public String accountSortField {get {if(accountSortField==null) accountSortField = 'Name'; return accountSortField;}
    set;}
    
    
    public String contactSortField {get {if(contactSortField == null) contactSortField = 'LastName'; return contactSortField;}
    set;}

    private String accQuery {get {if(accQuery == null) accQuery = 'SELECT id, Name FROM Account WHERE Name <> NULL '; return accQuery;} set;}
    
    private String conQuery {get {if(conQuery ==  null) conQuery = 'SELECT id, FirstName, LastName FROM Contact WHERE accountid <> NULL '; return conQuery; } set;}

    public void toggleAccountSort(){
        if(accountSortDirection.equals('asc')) accountSortDirection = 'desc';
        else 
            accountSortDirection = 'asc';
        runAccountQuery();
    }

    public void toggleContactSort(){
        if(contactSortDirection.equals('asc')) contactSortDirection = 'desc';
        else 
            contactSortDirection = 'asc';
        runContactQuery();
    }


    public SearchController(){
        runAccountQuery();
        runContactQuery();
    }
    
    
    
    public void runAccountQuery(){
        try{
            aSoql = accQuery + ' Order By ' + accountSortField + ' ' + accountSortDirection;
            aList = Database.query(accQuery + ' Order By ' + accountSortField + ' ' + accountSortDirection);
        } catch(Exception ex){system.debug(':<');ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()+ ' \t ' + ex.getStackTraceString()));}
    }
    
    public void runContactQuery(){
         try{
            cSoql = conQuery + ' Order By ' + contactSortField + ' ' + contactSortDirection;
            cList = Database.query(conQuery + ' Order By ' + contactSortField + ' ' + contactSortDirection);
        } catch(Exception ex){system.debug(':<');ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()+ ' \t ' + ex.getStackTraceString()));}
    }
    
    public void runAccountAndContactQuery(){
        String name = ApexPages.currentPage().getParameters().get('fname');
        if(name!=''){
            system.debug('SFDC name: '+name);
            accQuery = accQuery + ' and Name Like '+'\''+name+'%\'' ;
            conQuery = conQuery + ' and FirstName Like '+'\''+name+'%\'' ;
        }
        system.debug(accQuery );
        system.debug(conQuery);
        runAccountQuery();
        runContactQuery();
        
        
    }
    
    

    public void save(){
        if(accountRadioValue == null || contactRadioValue == null)
            return;
            
        
        
        Map<Id, Account> accountMap = new Map<Id, Account>(aList);
        Map<Id, Contact> contactMap = new Map<Id, Contact>(cList);
        
        if(accountRadioValue!='' || contactRadioValue != '') {
            Remarks__c r = new Remarks__c();
                
            Account acc = null;
            Contact con = null;
            if(accountRadioValue!='')
                acc = accountMap.get(Id.valueOf(accountRadioValue));
            if(contactRadioValue!='')
                con = contactMap.get(Id.valueOf(contactRadioValue));
                
            if(acc != null)
                r.Account__c = acc.id;
            if(con != null)
                r.Contact__c = con.id;
                
            if(accountInputValue != null)
                r.Review__c = accountInputValue;
            if(contactInputValue != null)
                r.Review__c = contactInputValue;    
            // try{
                insert r;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'Record Saved Successfully :)'));
            // }catch(Exception ex){ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()+ ' \t ' + ex.getStackTraceString() ));}
        }
       
        
        
        
    }

}
@isTest
public class TestSearchController{
    public class DataProvider{
        public List<Account> aList{get;set;}
        public List<Contact> cList{get;set;}
        
        public DataProvider(){
            aList = new List<Account>();
            cList = new List<Contact>();
        } // END init()
        
        public void createAccounts(){
            for(Integer i=0; i<10; i++){
                aList.add(
                	new Account ( name = 'Account-'+String.valueOf(i))
                );
            }
            insert aList;
        } // END public void createAccounts()   
        
        public void createContacts(){
            Integer counter=0;
            for(Account acc: aList) {
                counter++;
                cList.add(
                	new Contact(
                    	FirstName = 'FirstName-'+counter,
                        LastName = 'LastName-'+counter,
                        accountId = acc.id
                    )
                );
                
            }
            cList.add(
            	new Contact(
                	FirstName = 'Account-9',
                    LastName = 'last-9'
                )
            );
            insert cList;
        } // END public void createContacts()
    } // END public class DataProvider
    
    @isTest
        public static void unitTest() {
            DataProvider dp = new DataProvider();
            dp.createAccounts();
            dp.createContacts();
            SearchController sc = new SearchController();
            sc.toggleAccountSort();
            sc.toggleAccountSort();
            sc.toggleContactSort();
            sc.toggleContactSort();
            sc.accountRadioValue = String.valueOf(dp.aList[0].id);
            sc.contactRadioValue = String.valueOf(dp.cList[0].id);
            sc.contactInputValue = 'contact-Comment';
            sc.accountInputValue = 'account-Comment';
            sc.save();
            sc.accountRadioValue = null;
            sc.save();
			ApexPages.currentPage().getParameters().put('fname', 'Acc');
            sc.runAccountAndContactQuery();            
            sc.accountSortDirection = 'malformed-direction';
            sc.runAccountQuery();
            sc.contactSortDirection = 'malformed-direction';
            sc.runContactQuery();
            sc.accountRadioValue = String.valueOf('malformedVal');
            sc.contactRadioValue = String.valueOf('malFormedValue'); 
            
            
            
            sc.save();
        } // END public static void unitTest
    
    
    
}